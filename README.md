# Juice shop solver
OWASP - Juice shop solver

## How to install it
- Change the URL variable to your wanted juice shop (you can install it with https://github.com/bkimminich/juice-shop)
- Open the specified URL in a browser (if you want to see notifications and codes)
- Launch the script in Python
- Enjoy !

## Required librairies
- `requests` (native package in Python)
- `json` (native package in Python), only some challenges are concerned
- `concurrent.futures` (native package in Python >=3.2), only some challenges are concerned

## Licence
MIT

## Improvements
This project can be develop in the future. Don't hesitate to share your improvements, or to fork it ! To improve it, open  an issue or write me an email.

## Contributor
Bryan Fauquembergue
